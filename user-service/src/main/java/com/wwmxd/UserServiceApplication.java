package com.wwmxd;


import com.wwmxd.common.EnableLogAspect;
import com.wwmxd.wwmxdauth.EnablewwmxdAuthClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.kafka.annotation.EnableKafka;


@SpringBootApplication
@EnableDiscoveryClient
@EnablewwmxdAuthClient
@EnableFeignClients
@ServletComponentScan("com.wwmxd.config.druid")
@EnableLogAspect
@EnableKafka
public class UserServiceApplication {
	public static void main(String[] args) {
		SpringApplication.run(UserServiceApplication.class, args);
	}
}
