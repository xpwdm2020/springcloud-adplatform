package com.wwmxd.dao;

import com.wwmxd.entity.Operatelog;
import com.wwmxd.common.mapper.SuperMapper;

/**
 *
 * 
 *
 * @author WWMXD
 * @email 309980030@qq.com
 * @date 2018-03-02 16:25:03
 */
public interface OperatelogDao extends SuperMapper<Operatelog> {

}